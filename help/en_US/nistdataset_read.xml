<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from nistdataset_read.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="nistdataset_read" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>nistdataset_read</refname><refpurpose>Reads a dataset from NIST</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   data = nistdataset_read(filename)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>filename :</term>
      <listitem><para> a 1-by-1 matrix of strings, the name of the file to read</para></listitem></varlistentry>
   <varlistentry><term>data :</term>
      <listitem><para> a data structure containing the data from the file</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
The data variable has type NISTDTST and contains several fields.
The fields of the data structure are self-explanatory.
   </para>
   <para>
Some fields are mandatory, including:
name, category, description, reference.
   </para>
   <para>
Some files do not have datastring and model fields.
   </para>
   <para>
Depending on the category, some fields are optional.
   </para>
   <para>
If the category is "Nonlinear Least Squares Regression", then the following fields
are set: modelNumberOfParameters, modelEq, modelPar, modelFun,
residualSumOfSquares, residualStandardDev, degreeFreedom, numberOfObservations,
x, y, start1, start2, parameter, standarddeviation.
   </para>
   <para>
In the "Nonlinear Least Squares Regression" category, the
field "modelFun" is a function which evaluates the model
function.
The "modelFun" function has the header :
<screen>
y = modelFun(x,b)
</screen>
where x is a m-by-n matrix of doubles, the input of the model,
b is a 1-by-numberOfParameters matrix of doubles, the parameters,
and y is a m-by-n matrix of doubles, the output of the model.
For example, the "modelFun" field can be used to benchmark
regression methods or Non Linear Least Squares solvers.
   </para>
   <para>
If the category is "Univariate" or "Univariate: Summary Statistics",
then the following fields are set: sampleMean, sampleSTD, sampleAutocorr, y.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Read "datasets/nls/lower/Gauss2.dat"
path = nistdataset_getpath();
filename = fullfile(path,"datasets","nls","lower","Gauss2.dat");
data = nistdataset_read(filename)
// Plot the data
scf();
plot(data.x,data.y,"bo")
// See the number of parameters
data.modelNumberOfParameters
// Evaluates the model at one point
y = data.modelFun(data.x(1),data.parameter)
// Evaluates the model at all x points
y = data.modelFun(data.x,data.parameter);
// Plots the model against the data
h = scf();
plot(data.x,y,"r-");
plot(data.x,data.y,"bo");
legend(["Model","Data"]);
xtitle("Gauss2","X","Y");
h.children.children(3).children.thickness = 2;

// Read "datasets/univ/Lottery.dat"
path = nistdataset_getpath();
filename = fullfile(path,"datasets","univ","Lottery.dat");
data = nistdataset_read(filename)
// Compare the sample mean with the reference mean
data.sampleMean
mean(data.y)

// See the files in "datasets/univ"
path = nistdataset_getpath();
ls(fullfile(path,"datasets","univ"))

// See the files in "datasets/nls/lower"
path = nistdataset_getpath();
ls(fullfile(path,"datasets","nls","lower"))

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Statistical Reference Datasets, http://www.itl.nist.gov/div898/strd/</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 2011 - Torbjørn Pettersen</member>
   </simplelist>
</refsection>
</refentry>
