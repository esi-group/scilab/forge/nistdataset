// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- Non-regression test for bug 626 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nistdataset/issues/626/
//
// <-- Short Description -->
// nistdataset_read has an accuracy problem

//
path = nistdataset_getpath();
filename = fullfile(path,"datasets","nls","average","Kirby2.dat");
data = nistdataset_read(filename);
v = data.x(1);
assert_checkalmostequal(9.65E0,v,%eps);
