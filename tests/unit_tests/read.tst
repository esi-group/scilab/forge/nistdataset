// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Check errors
filename = "foo.txt";
instr = "data = nistdataset_read(filename)";
assert_checkerror ( instr , "nistdataset_read: Cannot open file foo.txt" );

//
// Check Gauss2
function y = gauss2Modelfun(x,b)
    // x: a m-by-n matrix of doubles, the input of the model
    // b: a 1-by-numberOfParameters matrix of doubles, the parameters
    // y: a m-by-n matrix of doubles, the output of the model
    y = b(1)*exp(-b(2)*x) ...
    + b(3)*exp(-(x-b(4)).^2./b(5).^2)...
    + b(6)*exp(-(x-b(7)).^2./b(8).^2)
endfunction

//
path = nistdataset_getpath();
filename = fullfile(path,"datasets","nls","lower","Gauss2.dat");
data = nistdataset_read(filename);
disp(data)
// Number of parameters in the model
assert_checkequal(data.modelNumberOfParameters,8);
// displays the model equation
modelEq = "y = b1 .* exp( -b2 .* x ) + b3 .* exp( -(x-b4) .^2  ./  b5 .^2 )+ b6 .* exp( -(x-b7) .^2  ./  b8 .^2 )";
assert_checkequal(data.modelEq,modelEq);
assert_checkequal(data.modelPar,"b1 b2 b3 b4 b5 b6 b7 b8");
// Check x
assert_checkequal(size(data.x),[250,1]);
assert_checkequal(round(data.x)-data.x,zeros(250,1));
assert_checktrue(data.x>=1);
assert_checktrue(data.x<=250);
// Check y
assert_checkequal(size(data.y),[250,1]);
assert_checktrue(data.y>=1.182677);
assert_checktrue(data.y<=133.8252);
// Check start1
assert_checkequal(size(data.start1),[8,1]);
// Check start2
assert_checkequal(size(data.start2),[8,1]);
// Check parameter
assert_checkequal(size(data.parameter),[8,1]);
// Check standarddeviation
assert_checkequal(size(data.standarddeviation),[8,1]);
// Evaluates the model at one point 
y = data.modelFun(data.x(1),data.parameter);
assert_checkalmostequal(y,97.935591,1.e-7);
// Evaluates the model at all x points
y1 = data.modelFun(data.x,data.parameter);
y2 = gauss2Modelfun(data.x,data.parameter);
assert_checkalmostequal(y1,y2);
// Plots the model    
if ( %f ) then
    h = scf();
    plot(data.x,y,"r-"); 
    // Plots the data
    plot(data.x,data.y,"bo"); 
    legend(["Model","Data"]);
    xtitle("Gauss2","X","Y");
    h.children.children(3).children.thickness = 2;
end
//
// Check Lottery
//
path = nistdataset_getpath();
filename = fullfile(path,"datasets","univ","Lottery.dat");
data = nistdataset_read(filename);
disp(data)
assert_checkequal(data.name,"Lottery");
assert_checkequal(data.category,"Univariate: Summary Statistics");
assert_checkequal(size(data.description),[8 1]);
assert_checkequal(size(data.datastring),[4 1]);
assert_checkequal(data.numberOfObservations,218);
assert_checkalmostequal(data.sampleMean,518.958715596330,%eps);
assert_checkalmostequal(data.sampleSTD,291.699727470969,%eps);
assert_checkalmostequal(data.sampleAutocorr,-0.120948622967393,%eps);
