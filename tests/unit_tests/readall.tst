// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
function [nbtotal,nberr] = readFilesInDir(dirn)
	// Read all .dat files in the given directory.
	// nbtotal: total number of .dat files in the directory.
	// nberr: total number of .dat files which could not be read.
    filemat = ls(dirn)';
    nbfiles = size(filemat,"c");
	nbtotal = 0
	nberr = 0
    for i = 1 : nbfiles
        filename = filemat(i);
        if ( fileext(filename) == ".dat" ) then
			nbtotal = nbtotal + 1;
            fp = fullfile(dirn,filename);
            instr = "data = nistdataset_read(fp)";
            ierr = execstr(instr,"errcatch");
            if (ierr == 0 ) then
				mprintf("[%d] Filename: %s\n",i,filename)
            else
				nberr = nberr + 1;
                mprintf("ERROR !\n");
                msg = lasterror();
                mprintf("%s\n",msg);
            end
        end
    end
endfunction


path = nistdataset_getpath();
//
dirn = fullfile(path,"datasets","nls","lower");
mprintf("\nReading files in nls/lower\n");
[nbtotal1,nberr1] = readFilesInDir(dirn);
//
dirn = fullfile(path,"datasets","nls","average");
mprintf("\nReading files in nls/average\n");
[nbtotal2,nberr2] = readFilesInDir(dirn);
//
dirn = fullfile(path,"datasets","nls","higher");
mprintf("\nReading files in nls/higher\n");
[nbtotal3,nberr3] = readFilesInDir(dirn);
//
dirn = fullfile(path,"datasets","univ");
mprintf("\nReading files in univ\n");
[nbtotal4,nberr4] = readFilesInDir(dirn);
//
assert_checkequal(nbtotal1+nbtotal2+nbtotal3+nbtotal4,34);
assert_checkequal(nberr1+nberr2+nberr3+nberr4,0);
