NIST Dataset toolbox

Purpose
-------

The goal of this toolbox is to provide a collection of datasets 
distributed by NIST.

The NIST Standard Reference Datasets is a collection of datasets. 
The purpose of this project is to improve the accuracy of statistical 
software by providing reference datasets with certified computational 
results that enable the objective evaluation of statistical software.

See the overview in the help provided with this toolbox.

Features
--------

The following is a list of functions in this toolbox.

 * nistdataset_getpath � Returns the path to the current module.
 * nistdataset_read � Reads a dataset from NIST
 
Moreover, the module provides 34 datasets from NIST in the following 
categories:
 * Univariate Summary Statistics (9 datasets)
 * Non Linear Least Squares (25 datasets)

Datasets from other categories are provided on the NIST 
website, which cannot be read by the current toolbox. 
However, it should be straightforward to extend the current 
toolbox to read the other categories of files.
 
Dependencies
------------

 * This module depends on the assert module.
 * This module depends on the apifun module.
 * This module depends on the helptbx module.

Author
------

 * Copyright (C) 2012 - Michael Baudin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin

Licence
-------

This toolbox is distributed under the CeCILL license.

Bibliography
------------

 * Statistical Reference Datasets, http://www.itl.nist.gov/div898/strd/

TODO
----
 * Extend the data reading to Analysis of Variance
 * Extend the data reading to Linear Regression
 * Extend the data reading to Markov Chain Monte Carlo
 * Create functions to compute the value for Non Linear Least Squares
 
 