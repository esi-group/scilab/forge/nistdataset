// Copyright (C) 2011 - Torbjørn Pettersen
// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function data = nistdataset_read(filename)
    // Reads a dataset from NIST
    //
    // Calling Sequence
    // data = nistdataset_read(filename)
    //
    // Parameters
    // filename : a 1-by-1 matrix of strings, the name of the file to read
    // data : a data structure containing the data from the file
    //
    // Description
    // The data variable has type NISTDTST and contains several fields. 
    // The fields of the data structure are self-explanatory.
    //
    // Some fields are mandatory, including: 
    // name, category, description, reference.
    //
    // Some files do not have datastring and model fields.
    //
    // Depending on the category, some fields are optional.
    //
    // If the category is "Nonlinear Least Squares Regression", then the following fields 
    // are set: modelNumberOfParameters, modelEq, modelPar, modelFun, 
    // residualSumOfSquares, residualStandardDev, degreeFreedom, numberOfObservations, 
    // x, y, start1, start2, parameter, standarddeviation.
    //
    // In the "Nonlinear Least Squares Regression" category, the 
    // field "modelFun" is a function which evaluates the model 
    // function. 
    // The "modelFun" function has the header :
    // <screen>
    // y = modelFun(x,b)
    // </screen>
    // where x is a m-by-n matrix of doubles, the input of the model, 
    // b is a 1-by-numberOfParameters matrix of doubles, the parameters, 
    // and y is a m-by-n matrix of doubles, the output of the model. 
    // For example, the "modelFun" field can be used to benchmark  
    // regression methods or Non Linear Least Squares solvers.
    //
    // If the category is "Univariate" or "Univariate: Summary Statistics", 
    // then the following fields are set: sampleMean, sampleSTD, sampleAutocorr, y.
    //
    // Examples
    // // Read "datasets/nls/lower/Gauss2.dat"
    // path = nistdataset_getpath();
    // filename = fullfile(path,"datasets","nls","lower","Gauss2.dat");
    // data = nistdataset_read(filename)
    // // Plot the data
    // scf();
    // plot(data.x,data.y,"bo")
    // // See the number of parameters
    // data.modelNumberOfParameters
    // // Evaluates the model at one point 
    // y = data.modelFun(data.x(1),data.parameter)
    // // Evaluates the model at all x points
    // y = data.modelFun(data.x,data.parameter);
    // // Plots the model against the data
    // h = scf();
    // plot(data.x,y,"r-"); 
    // plot(data.x,data.y,"bo"); 
    // legend(["Model","Data"]);
    // xtitle("Gauss2","X","Y");
    // h.children.children(3).children.thickness = 2;
    //
    // // Read "datasets/univ/Lottery.dat"
    // path = nistdataset_getpath();
    // filename = fullfile(path,"datasets","univ","Lottery.dat");
    // data = nistdataset_read(filename)
    // // Compare the sample mean with the reference mean
    // data.sampleMean
    // mean(data.y)
    //
    // // See the files in "datasets/univ"
    // path = nistdataset_getpath();
    // ls(fullfile(path,"datasets","univ"))
    //
    // // See the files in "datasets/nls/lower"
    // path = nistdataset_getpath();
    // ls(fullfile(path,"datasets","nls","lower"))
    //
    // Bibliography
    //  Statistical Reference Datasets, http://www.itl.nist.gov/div898/strd/
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) 2011 - Torbjørn Pettersen

    [lhs,rhs]=argn()
    apifun_checkrhs ( "nistdataset_read" , rhs , 1:1 )
    apifun_checklhs ( "nistdataset_read" , lhs , 0:1 )
    //
    //
    // Check type
    apifun_checktype ( "nistdataset_read" , filename , "filename" , 1 , "string")
    //
    // Read the file
    [fd,err]=mopen(filename);
    if ( err <> 0 ) then
        // Temporary workaround for http://bugzilla.scilab.org/show_bug.cgi?id=4833
        //error(msprintf(gettext("%s: Cannot open file %s.\n"),"nistdataset_read",filename))
        error(msprintf(gettext("%s: Cannot open file \n"),"nistdataset_read")+filename)
    end
    content = mgetl(fd);
    err = mclose(fd);
    if ( err <> 0 ) then
        // Temporary workaround for http://bugzilla.scilab.org/show_bug.cgi?id=4833
        //error(msprintf(gettext("%s: Cannot close file %s.\n"),"nistdataset_read",filename))
        error(msprintf(gettext("%s: Cannot close file \n"),"nistdataset_read")+filename)
    end
    // Clean blanks
    content = stripblanks(content);
    //
    // Get the Dataset Name
    ipatt = searchpattern(content,"/Dataset Name: .* \(.*\)/");
    if ( ipatt<>0 ) then
        str = stripblanks(content(ipatt));
        [n,name,fn] = msscanf(str,"Dataset Name: %s %s");
    else
        ipatt = searchpattern(content,"/Dataset Name: .*/");
        if ( ipatt<>0 ) then
            str = stripblanks(content(ipatt));
            [n,name,fn] = msscanf(str,"Dataset Name: %s %s");
        end
    end
    if ( ipatt == 0 ) then
        unableToRead(filename,"Dataset Name");
    end
    //
    // Get Certified Values range
    ipatt = searchpattern(content,"/\s*Certified Values\s*\(lines .* to .*\)\s*/");
    if ( ipatt <> 0 ) then
        str = stripblanks(content(ipatt));
        [n,ignored1,ignored2,iCertifStart,ignored3,iCertifEnd] = msscanf(str,"%s %s (lines %d %s %d)");
    else
        ipatt = searchpattern(content,"/\s*Certified Values:\s*lines .* to .* .*\s*/");
        if ( ipatt <> 0 ) then
            str = stripblanks(content(ipatt));
            [n,ignored1,ignored2,iCertifStart,ignored3,iCertifEnd] = msscanf(str,"%s %s lines %d %s %d");
        end
    end
    if ( ipatt == 0 ) then
        unableToRead(filename,"Certified Values");
    end
    //
    // Get Data Range
    ipatt = searchpattern(content,"/\s*Data\s*\(lines .* to .*\)\s*/");
    if ( ipatt<>0 ) then
        str = stripblanks(content(ipatt));
        [n,ignored1,iDataStart,ignored2,iDataEnd] = msscanf(str,"%s (lines %d %s %d)");
    else
        ipatt = searchpattern(content,"/\s*Data\s*:\s*lines .* to .* .*/");
        str = stripblanks(content(ipatt));
        [n,ignored1,iDataStart,ignored2,iDataEnd] = msscanf(str,"%s : lines %d %s %d)");
    end
    if ( ipatt == 0 ) then
        unableToRead(filename,"Data");
    end
    //
    // Get "Procedure" or "Stat Category"
    ipatt = searchpattern(content,"/\s*Procedure:.*/");
    if ( ipatt<>0 ) then
        category = stripblanks(content(ipatt));
        category = getaftercomma(category);
    else
        ipatt = searchpattern(content,"/\s*Stat Category:.*/");
        category = stripblanks(content(ipatt));
        category = getaftercomma(category);
    end
    if ( ipatt == 0 ) then
        unableToRead(filename,"Procedure or Stat Category");
    end
    //
    // Get Description
    ipatt = searchpattern(content,"/\s*Description:.*/");
    if ( ipatt == 0 ) then
        unableToRead(filename,"Description");
    end
    kstop = ipatt + find(content(ipatt:$)=="",1);
    description = content(ipatt:kstop-1);
    description(1)=getaftercomma(description(1));
    description(description=="")=[];
    //
    // Get Reference
    ipatt = searchpattern(content,"/\s*Reference:.*/")
    if ( ipatt == 0 ) then
        unableToRead(filename,"Reference")
    end
    kstop = ipatt + find(content(ipatt:$)=="",1)
    reference = content(ipatt:kstop-1)
    reference(1) = getaftercomma(reference(1))
    reference(reference=="")=[]
    //
    // Get Data
    ipatt = searchpattern(content,"/\s*Data:.*/");
    if ( ipatt == 0 ) then
        datastring = ""
    else
        kstop = ipatt + find(content(ipatt:$)=="",1);
        datastring = content(ipatt:kstop-1);
        datastring(1) = getaftercomma(datastring(1));
        datastring(datastring=="")=[];
    end
    //
    // Get Model
    // Get modelNumberOfParameters
    ipatt = searchpattern(content,"/\s*Model:.*/");
    if ( ipatt == 0 ) then
        model = ""
        modelNumberOfParameters = []
    else
        kstop = ipatt + find(content(ipatt:$)=="",1);
        model = content(ipatt:kstop-1);
        model(1) = getaftercomma(model(1));
        // Set Number of Parameters
        str = stripblanks(content(ipatt+1));
        [n,modelNumberOfParameters,firstParam,lastParam] = msscanf(str,"%d Parameters (b%d to b%d)")
    end
    //
    // Get the regression model 
    // (assuming that it is stuck between 'y =' and '+ e')
    ipatt = searchpattern(content,"/^y\s*=.*/");
    if ( ipatt == 0 ) | ..
        ( category == "Univariate" | ..
        category == "Univariate: Summary Statistics" ) then
        modelEq = "y = mu";
        modelPar= "mu";
        funheader = "y=modelFun(x,mu)"
        funbody = "y=mu;"
        deff(funheader,funbody)
    else
        kstop = ipatt + searchpattern(content(ipatt:$),"/\+\s*e$/");
        modelEq = content(ipatt:kstop-1);
        // arrange on one line
        modelEq = stripblanks(strcat(modelEq));         
        // chop of " + e" at the end
        modelEq = strsubst(modelEq,"/\+\s*e$/","","r"); 
        // substitue ** for .^
        modelEq = strsubst(modelEq,"**"," .^"); 
        // substitue * for .*
        modelEq = strsubst(modelEq,"*"," .* "); 
        // substitue / for ./
        modelEq = strsubst(modelEq,"/"," ./ "); 
        // substitue [ for (
        modelEq = strsubst(modelEq,"[","("); 
        modelEq = strsubst(modelEq,"]",")"); 
        modelEq = stripblanks(modelEq);         
        // space separated parameter names
        modelPar = sprintf("b%i ",[1:modelNumberOfParameters]'); 
        modelPar = stripblanks(modelPar);         
        // y = modelFun(x,b)
        funheader = "y=modelFun(x,b)"
        funbody = nlinregr_formula(modelEq,modelPar,"b(%i)")
        deff(funheader,funbody); 
    end
    //
    // Set default values of optional fields
    residualSumOfSquares=[]
    residualStandardDev=[]
    degreeFreedom=[]
    numberOfObservations=[]
    x=[]
    y=[]
    start1=[]
    start2=[]
    parameter=[]
    standdev=[]
    sampleMean=[]
    sampleSTD=[]
    sampleAutocorr=[]
    //
    if ( category == "Nonlinear Least Squares Regression" ) then
        [residualSumOfSquares,residualStandardDev,degreeFreedom,...
        numberOfObservations,x,y,start1,start2,parameter,..
        standdev] = readNLS(filename,content,iCertifStart,iCertifEnd,..
        iDataStart,iDataEnd);
    elseif ( category == "Univariate" | ..
        category == "Univariate: Summary Statistics" ) then
        [sampleMean,sampleSTD,sampleAutocorr,...
        numberOfObservations, y] = readUnivariate(filename,...
        content,iCertifStart,iCertifEnd,iDataStart,iDataEnd);
    else
        lclmsg = gettext("%s: Cannot read file with category %s\n")
        error(msprintf(lclmsg,"nistdataset_read",category))
    end
    //
    // Create the structure
    data = tlist(["NISTDTST","x","y","name","start1","start2","parameter",..
    "standarddeviation","category","description","reference",...
    "datastring","model","modelNumberOfParameters","modelEq","modelPar","modelFun",..
    "residualSumOfSquares","residualStandardDev",..
    "degreeFreedom","numberOfObservations","sampleMean",..
    "sampleSTD","sampleAutocorr"]);
    //
    // Set into x and y the input and output measures
    data.y = y;
    data.x = x;
    data.name = name;
    data.start1 = start1;
    data.start2 = start2;
    data.parameter = parameter;
    data.standarddeviation = standdev;
    data.category = category;
    data.description = description;
    data.reference = reference;
    data.datastring = datastring;
    data.model = model;
    data.modelNumberOfParameters = modelNumberOfParameters;
    data.modelEq = modelEq;
    data.modelPar = modelPar;
    data.modelFun = modelFun;
    data.residualSumOfSquares = residualSumOfSquares;
    data.residualStandardDev = residualStandardDev;
    data.degreeFreedom = degreeFreedom;
    data.numberOfObservations = numberOfObservations;
    data.sampleMean = sampleMean;
    data.sampleSTD = sampleSTD;
    data.sampleAutocorr = sampleAutocorr;
endfunction

function [residualSumOfSquares,residualStandardDev,degreeFreedom,...
    numberOfObservations,x,y,start1,start2,parameter,...
    standdev] = readNLS(filename,content,iCertifStart,iCertifEnd,iDataStart,iDataEnd)
    //
    // Reading file for Non Linear Least Squares
    //

    //
    pattern = "/\s*Residual Sum of Squares:.*/"
    keyword = "Residual Sum of Squares"
    residualSumOfSquares  = readDoubleData(filename,content,pattern,keyword)
    //
    pattern = "/\s*Residual Standard Deviation:.*/"
    keyword = "Residual Standard Deviation"
    residualStandardDev = readDoubleData(filename,content,pattern,keyword)
    // 
    pattern = "/\s*Degrees of Freedom:.*/"
    keyword = "Degrees of Freedom"
    degreeFreedom = readDoubleData(filename,content,pattern,keyword)
    //
    pattern = "/\s*Number of Observations:.*/"
    keyword = "Number of Observations"
    numberOfObservations = readDoubleData(filename,content,pattern,keyword)
    //
    // Get data
    table = content(iDataStart:iDataEnd);
    table = stripblanks(table);
    table=msscanf(-1,table,"%lg %lg");
    y = table(:,1);
    x = table(:,2);
    //
    // Get certified values
    table = content(iCertifStart:iCertifEnd);
    nrows = size(table,"r")
    start1 = []
    start2 = []
    parameter = []
    standdev = []
    for i = 1 : nrows
        str = table(i)
        [n,ignored1,x1,x2,x3,x4] = msscanf(str,"%s = %lg %lg %lg %lg");
        if ( n==5 ) then
            start1($+1) = x1;
            start2($+1) = x2;
            parameter($+1) = x3;
            standdev($+1) = x4;
        end
    end
endfunction
function outstr = getaftercomma(str)
    // Returns what is after the comma ":"
    i = strindex(str,":")
    i = i(1)
    len = length(str)
    outstr = part(str,i+1:len)
    outstr = stripblanks(outstr)
endfunction
function iline = searchpattern(content,pattern)
    // Search for a line matching the pattern 
    // in the matrix of strings "content", and 
    // return the line index, if any.
    // If no line matches, returns 0.
    nbrows = size(content,"r")
    iline = 0
    for i = 1 : nbrows
        [st,en,match]=regexp(content(i),pattern);
        if ( st <> [] ) then
            iline = i
            break
        end
    end
endfunction
function unableToRead(filename,keyword)
    // Produces an error if the keyword was not found in filename 
    // Temporary workaround for http://bugzilla.scilab.org/show_bug.cgi?id=4833
    //error(msprintf(gettext("%s: Unable to read %s"),"nistdataset_read",filename))
    part1 = msprintf(gettext("%s: Unable to read "),"nistdataset_read")
    part2 = msprintf(gettext(" for keyword %s"),keyword)
    error(part1+filename+part2)
endfunction
function x = readDoubleData(filename,content,pattern,keyword)
    // Search for the pattern in the file.
    // If no line matches this pattern, produces an error.
    // If one line matches the pattern, returns whatever is after the comma.
    ipatt = searchpattern(content,pattern)
    if ( ipatt == 0 ) then
        unableToRead(filename,keyword)
    end
    str = content(ipatt)
    str = getaftercomma(str)
    x = msscanf(str,"%lg")
endfunction

function [sampleMean,sampleSTD,sampleAutocorr,...
    numberOfObservations, y] = readUnivariate(filename,...
    content,iCertifStart,iCertifEnd,iDataStart,iDataEnd)
    //
    // Reading file for Univariate
    //

    //
    pattern = "/\s*Sample Mean.*/"
    keyword = "Sample Mean"
    sampleMean = readDoubleData(filename,content,pattern,keyword)
    //
    pattern = "/\s*Sample Standard Deviation.*/"
    keyword = "Sample Standard Deviation"
    sampleSTD = readDoubleData(filename,content,pattern,keyword)
    //
    pattern = "/\s*Sample Autocorrelation Coefficient.*/"
    keyword = "Sample Autocorrelation Coefficient"
    sampleAutocorr = readDoubleData(filename,content,pattern,keyword)
    //
    pattern = "/\s*Number of Observations:.*/"
    keyword = "Number of Observations"
    numberOfObservations = readDoubleData(filename,content,pattern,keyword)
    //
    // Get data
    table = content(iDataStart:iDataEnd);
    table = stripblanks(table);
    y=msscanf(-1,table,"%lg");
endfunction


function new_expr=nlinregr_formula(expr,old_names,new_nameformat)
    // private function which replace parameter 
    // names with p(1),...p(n) and 
    // independent variables with data(:,1),...data(:,i). 
    // Used to create the curve function.
    expr=stripblanks(expr);
    Names=tokens(old_names);
    str="[\=\.\^\+\-\(\)\*\/\\,]";
    for i=1:size(Names,"*"),
        // parameter alone in front
        [m,n]=regexp(expr,"/^"+Names(i)+"/"); 
        if ~isempty(m) then 
            expr=sprintf(new_nameformat,i)+part(expr,[n+1:length(expr)]); 
        end
        // parameter in the middle
        [m,n]=regexp(expr,"/"+str+"\s*"+Names(i)+"\s*"+str+"/"); 
        // continue until all occurences have been replaced
        while ~isempty(m) 
            expr=part(expr,[1:m(1)])+..
            sprintf(new_nameformat,i)+..
            part(expr,[n(1):length(expr)]);
            // parameter in the middle
            [m,n]=regexp(expr,"/"+str+Names(i)+str+"/"); 
        end
        // parameter alone in the back
        [m,n]=regexp(expr,"/"+Names(i)+"$/"); 
        if ~isempty(m) then 
            expr=part(expr,[1:m-1])+sprintf(new_nameformat,i); 
        end
    end  
    new_expr=expr; 
endfunction
